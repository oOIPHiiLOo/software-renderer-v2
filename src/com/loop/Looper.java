package com.loop;

public interface Looper {
    void call(double delta);
}
