package com.loop;

public class Loop implements Runnable {
    private Thread thread;
    private Looper looper;
    private boolean looping;
    private int cap;

    public Loop(int cap, Looper looper) {
        this.thread = new Thread(this);
        this.cap = cap;
        this.looper = looper;
    }

    public synchronized void begin() {
        this.looping = true;
        this.thread.start();
    }

    public synchronized void end() throws InterruptedException {
        this.looping = false;
        this.thread.join();
    }

    @Override
    public void run() {
        double nanoSecondsPerFrame = 1000000000.0 / this.cap;
        long currentFrameDuration = 0;
        long lastMeasurement = System.nanoTime();

        while (this.looping) {
            long currentMeasurement = System.nanoTime();
            long currentLoopDelta = currentMeasurement - lastMeasurement;

            lastMeasurement = currentMeasurement;
            currentFrameDuration += currentLoopDelta;

            if (currentFrameDuration >= nanoSecondsPerFrame) {
                this.looper.call(currentLoopDelta > nanoSecondsPerFrame ? currentLoopDelta / nanoSecondsPerFrame : 0);
                currentFrameDuration -= nanoSecondsPerFrame;
            }
        }
    }

    public static double interpolate(double delta, double value) {
        if (delta > 0) {
            return delta * value;
        } else {
            return value;
        }
    }
}
