package com.engine;

import com.loop.Loop;
import com.loop.Looper;

public class Main implements Looper {
    private int frameCount;

    private Main() {
        this.frameCount = 0;
    }

    public static void main(String[] args) {
        Loop l = new Loop(60, new Main());

        l.begin();
    }

    @Override
    public void call(double delta) {
        this.frameCount++;
        System.out.printf("value per tick: %f\n", Loop.interpolate(delta, 20));
        try {
            Thread.sleep(33);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
